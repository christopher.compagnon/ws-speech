<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet 
version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/1999/xhtml"
> 
<!-- ========================== Imports =========================== -->

<!--  pour utiliser le javascript : -->
<xsl:output
method="html"
doctype-system="about:legacy-compat"
omit-xml-declaration="yes"
encoding="utf-8"
indent="no"/>

<!-- ========================== Définitions des variables =========================== -->
<xsl:variable name="notifications" select="application/notifications/notification"/>
<xsl:variable name="data" select="application/data"/>
<xsl:variable name="params" select="application/params"/>
<xsl:variable name="action" select="$params/action"/>
<xsl:variable name="locked" select="$params/locked"/>
<xsl:variable name="updates.allow" select="$params/AllowUpdates"/>
<!-- ========================== Règles =========================== -->
<xsl:variable name="first.config">
<xsl:choose>
<xsl:when test="$data/EndPointID/@status = 'false'">true</xsl:when>
<xsl:otherwise>false</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<!-- ========================== Fonctions =========================== -->

<!-- ========================== Template principal =========================== -->
<xsl:template match="/">
<html class="application">

<head>
<title>Settings</title>
<link rel="icon" href="cog.svg" />
<!-- <link rel="stylesheet" type="text/css" href="./css/driver.css"/> -->
<meta content="application/xhtml+xml; charset=utf-8" http-equiv="content-type" />
<meta content="firenode.net" name="author" />
<meta content="VSCode" name="generator" />
<link rel="stylesheet" type="text/css" href="./css/app.config.css" />
<link rel="stylesheet" type="text/css" href="./css/fontawesome/css/all.css" />
<!-- <script type="text/javascript" src="./js/notification.js"></script> -->

<!-- Gestion des notifications -->
<script type="text/javascript">
<xsl:text>
/* Notifications */
</xsl:text>
<xsl:for-each select="$notifications">
<xsl:choose>
<xsl:when test="@type = 'MsgSessionExpiredError'">alert('The session has expired.'); </xsl:when>
<xsl:when test="@type = 'MsgSessionInvalidError'">alert('Invalid session.'); </xsl:when>
<xsl:when test="@type = 'MsgIPInvalidError'">alert('IP address invalid.'); </xsl:when>
<xsl:when test="@type = 'MsgPassphraseInvalidError'">alert('Passphrase invalid.'); </xsl:when>
<xsl:when test="@type = 'MsgUpdateDeployedSuccess'">alert('Update successfully deployed.'); </xsl:when>
</xsl:choose>
</xsl:for-each>
</script>
</head>

<body>

<!-- Titre -->
<header>
<h1>Settings</h1>

</header>

<!-- Affichage principal -->
<xsl:call-template name="page"/>

<footer>
<p>Conception, Design &#38; Infrastructure : firenode.net &#x25C6; <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/fr/legalcode" title="licence creative commons complète" target="_blank" rel="licence external">Licence</a></p>
</footer>

</body>
</html>
</xsl:template>

<!-- =========================================================================== -->
<xsl:template name="page">
<main>	

<!-- Affichage page -->

<form method="POST" action="app.config.sh">
<table class="app-display">

<colgroup>
<col width="10%"/>
<col width="40%"/>
<col width="40%"/>
<col width="10%"/>
</colgroup>

<thead>

<tr>
<th></th>
<th><p>Endpoint</p></th>
<th></th>
<th>
<xsl:variable name="action.connection">
<xsl:choose>
<xsl:when test="$first.config = 'true'">defineConnection</xsl:when>
<xsl:otherwise>getConnection</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<input type="hidden" name="action" value="{$action.connection}"></input>
<xsl:if test="$locked != 'true'"><input type="submit" value="OK"></input></xsl:if>
</th>
</tr>

</thead>

<tbody>
	
<!-- EndPointID -->
<tr>
<th></th>
<th><p>ID</p></th>
<th><p><xsl:value-of select="$data/EndPointID"/></p></th>
<th></th>
</tr>

<xsl:if test="$locked != 'true' or $first.config = 'true'">
<!-- Mot de passe -->
<tr>
<th></th>
<th><p><label for="Passphrase1">Access passphrase</label></p></th>
<th><input type="password" id="Passphrase1" name="Passphrase1" required="required"></input></th>
<th></th>
</tr>
</xsl:if>

<!-- Mot de passe (bis) uniquement si c'est la première configuration -->
<xsl:if test="$first.config = 'true'">
<tr>
<th></th>
<th><p><label for="Passphrase2">Passphrase (bis)</label></p></th>
<th><input type="password" id="Passphrase2" name="Passphrase2" required="required"></input></th>
<th></th>
</tr>
</xsl:if>

</tbody>

<tfoot>

<!-- cadenas : affichage inverse de l'habitude ; si écriture possible, alors verrou, sinon pas verrou -->
<tr>
<th><p>
<xsl:choose>
<xsl:when test="$locked = 'false'"><a href="app.config.sh?action=lock"><i class="fas fa-lock"></i></a></xsl:when>
<xsl:otherwise><a href="app.config.sh?action=unlock"><i class="fas fa-lock-open"></i></a></xsl:otherwise>
</xsl:choose>
</p></th>
<th></th>
<th></th>
<th></th>
</tr>

</tfoot>
	
</table>
</form>

<xsl:if test="$locked = 'true'">

<!-- Restriction par adresse IP -->
<xsl:variable name="RemoteAddress.value" select="$data/RemoteAddress" />
<xsl:variable name="RemoteAddress.version" select="$data/RemoteAddress/@version" />
<xsl:variable name="AccessRestrictionByIP" select="$params/AccessRestrictionByIP" />
<xsl:variable name="AccessRestrictionByIP.value" select="$data/IPv4AddressRestriction" />
<xsl:variable name="AccessRestrictionByIP.mask" select="$data/IPv4AddressRestriction/@mask" />
<xsl:variable name="IPv4.part1">
<xsl:choose>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'false'"><xsl:value-of select="substring-before($RemoteAddress.value,'.')" /></xsl:when>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'true'"><xsl:value-of select="substring-before($AccessRestrictionByIP.value,'.')" /></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="IPv4.rest1">
<xsl:choose>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'false'"><xsl:value-of select="substring-after($RemoteAddress.value,'.')" /></xsl:when>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'true'"><xsl:value-of select="substring-after($RemoteAddress.value,'.')" /></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="IPv4.part2">
<xsl:choose>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'false'"><xsl:value-of select="substring-before($IPv4.rest1,'.')" /></xsl:when>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'true'"><xsl:value-of select="substring-before($IPv4.rest1,'.')" /></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="IPv4.rest2">
<xsl:choose>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'false'"><xsl:value-of select="substring-after($IPv4.rest1,'.')" /></xsl:when>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'true'"><xsl:value-of select="substring-after($IPv4.rest1,'.')" /></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="IPv4.part3">
<xsl:choose>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'false'"><xsl:value-of select="substring-before($IPv4.rest2,'.')" /></xsl:when>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'true'"><xsl:value-of select="substring-before($IPv4.rest2,'.')" /></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="IPv4.part4">
<xsl:choose>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'false'"><xsl:value-of select="substring-after($IPv4.rest2,'.')" /></xsl:when>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'true'"><xsl:value-of select="substring-after($IPv4.rest2,'.')" /></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="IPv4.mask">
<xsl:choose>
<xsl:when test="$RemoteAddress.version = 4 and $AccessRestrictionByIP = 'true'"><xsl:value-of select="$AccessRestrictionByIP.mask" /></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<hr class="app-separe" />
<form method="POST" action="app.config.sh" id="setIPRestriction">
<table class="app-display">

<colgroup>
<col width="10%" />
<col width="40%" />
<col width="40%" />
<col width="10%" />
</colgroup>

<thead>
<tr>
<th><xsl:if test="$params/AccessRestrictionByIP = 'true'"><i class="fas fa-check"></i></xsl:if></th>
<th><p>IP restriction (for web interface)</p></th>
<th></th>
<th><input type="hidden" name="action" value="setIPRestriction" />
<input type="submit" value="OK" /></th>
</tr>
</thead>

<tbody>
<tr>
<th></th>
<th><p><label for="IPv4_1">IPv4 address</label></p></th>
<th><p><input type="number" id="IPv4_1" name="IPv4_1" min="1" max="255" form="setIPRestriction" value="{$IPv4.part1}" required="required" />.<input type="number" id="IPv4_2" name="IPv4_2" min="0" max="255" form="setIPRestriction" value="{$IPv4.part2}" required="required" />.<input type="number" min="0" max="255" id="IPv4_3" name="IPv4_3" form="setIPRestriction" value="{$IPv4.part3}" required="required" />.<input type="number" min="0" max="255" id="IPv4_4" name="IPv4_4" form="setIPRestriction" value="{$IPv4.part4}" required="required" /> / <input type="number" id="IPv4_mask" name="IPv4_mask" min="1" max="32" form="setIPRestriction" value="{$AccessRestrictionByIP.mask}" required="required" /></p></th>
<th></th>
<th></th>
</tr>
<!-- IPv6 non actif 
<tr>
<th></th>
<th><p><label for="IPv6_1">IPv6 address</label></p></th>
<th><p><input type="text" size="2" maxlength="4" id="IPv6_1" name="IPv6_1" form="setIPRestriction" />:<input type="text" size="2" maxlength="4" id="IPv6_2" name="IPv6_2" form="setIPRestriction" />:<input type="text" size="2" maxlength="4" id="IPv6_3" name="IPv6_3" form="setIPRestriction" />:<input type="text" size="2" maxlength="4" id="IPv6_4" name="IPv6_4" form="setIPRestriction" />:<input type="text" size="2" maxlength="4" id="IPv6_5" name="IPv6_5" form="setIPRestriction" />:<input type="text" size="2" maxlength="4" id="IPv6_6" name="IPv6_6" form="setIPRestriction" />:<input type="text" size="2" maxlength="4" id="IPv6_7" name="IPv6_7" form="setIPRestriction" />:<input type="text" size="2" maxlength="4" id="IPv6_8" name="IPv6_8" form="setIPRestriction" /> / <select id="IPv6_mask" name="IPv6_mask" form="setIPRestriction">
<option value="64">64</option>
<option value="80">80</option>
<option value="96">96</option>
<option value="112">112</option>
<option value="128">128</option>
</select></p></th>
<th></th>
</tr>
-->
</tbody>

</table>
</form>
	
<hr class="app-separe"/>
<table class="app-display">

<colgroup>
<col width="10%"/>
<col width="40%"/>
<col width="40%"/>
<col width="10%"/>
</colgroup>

<thead>

<tr>
<th></th>
<th><p>Application settings</p></th>
<th></th>
<th></th>
</tr>

</thead>

<tbody>

<!-- Définition du Vault :
Le fichier de Vault contient le mot de passe du EndPoint
Il n'est accessible qu'en lecture seule et ne peut pas être modifié depuis l'interface.
Il faut l'intervention manuelle du root sur la machine.
-->	
	
<!-- Définition du Vault :
Le fichier de Vault contient le mot de passe du EndPoint
Il n'est acessible qu'en lecture seule et ne peut pas être modifié depuis l'interface.
Il faut l'intervention manuelle du root sur la machine.
-->

<tr>
<th></th>
<th><p><label for="VAULT_FILE">Vault</label></p></th>
<th><p><input type="text" id="VAULT_FILE" name="VAULT_FILE" value="{$data/Vault}" required="required" form="setVault" disabled="disabled"></input></p></th>
<th>
<form method="POST" action="app.config.sh" id="setVault">
<!--
<input type="hidden" name="action" value="setVault"></input>
<input type="submit" value="OK"></input>
 -->
</form>
</th>
</tr>



<!-- Répertoire applicatif -->
<tr>
<th><xsl:if test="$data/AppDir/@status = 'true'"><i class="fas fa-check"></i></xsl:if></th>
<th><p><label for="APP_DIR">Application &amp; data directory</label></p></th>
<th><p><input type="text" id="APP_DIR" name="APP_DIR" value="{$data/AppDir}" required="required" form="setAppDir" disabled="disabled"></input></p></th>
<th>
<form method="POST" action="app.config.sh" id="setAppDir">
<!--
<input type="hidden" name="action" value="setAppDir"></input>
<input type="submit" value="OK"></input>
 -->
</form>
</th>
</tr>


<!-- URL publique -->
<tr>
<th><xsl:if test="$data/EndPointURL/@status = 'true' or $data/EndPointURL != ''"><i class="fas fa-check"></i></xsl:if></th>
<th><p><label for="APP_URL">Public URL</label></p></th>
<th><p><input type="url" id="APP_URL" name="APP_URL" value="{$data/EndPointURL}" required="required" form="setEndPointURL"></input></p></th>
<th>
<form method="POST" action="app.config.sh" id="setEndPointURL">
<input type="hidden" name="action" value="setEndPointURL"></input>
<input type="submit" value="OK"></input>
</form>
</th>
</tr>


<!-- Durée de rétention des logs -->
<tr>
<th><i class="fas fa-check"></i></th>
<th><p><label for="LOG_RETENTION_MAX">Logs retention (days)</label></p></th>
<th><p><input type="number" id="LOG_RETENTION_MAX" name="LOG_RETENTION_MAX" value="{$data/MaxLogsRetention}" required="required" form="setMaxLogsRetention" step="1" min="1" max="100"></input></p></th>
<th>
<form method="POST" action="app.config.sh" id="setMaxLogsRetention">
<input type="hidden" name="action" value="setMaxLogsRetention"></input>
<input type="submit" value="OK"></input>
</form>
</th>
</tr>

<!-- Code d'accès au webservice -->
<tr>
<th><xsl:if test="$data/Passcode != ''"><i class="fas fa-check"></i></xsl:if></th>
<th><p><label for="PASSCODE">Passcode (authentication for webservice)</label></p></th>
<th><p><xsl:value-of select="$data/Passcode"/></p>
<p></p></th>
<th>
<form method="POST" action="app.config.sh" id="GenNewPassode">
<input type="hidden" name="action" value="GenNewPassode"></input>
<input type="submit" value="Refresh"></input>
</form>
</th>
</tr>

<tr>
<th></th>
<td colspan="3">
<p><strong>Notes:</strong></p>
<p>The client (for Linux) can be downloaded at <a href="{$data/EndPointURL}/client.tar"><xsl:value-of select="$data/EndPointURL" />/client.tar</a>.</p>
<p>The webservice must be requested at <i><xsl:value-of select="$data/EndPointURL" />/ws.speech.sh</i>.</p>
</td>
</tr>

</tbody>
</table>

<hr class="app-separe"/>

<table class="app-display">

<colgroup>
<col width="10%"/>
<col width="40%"/>
<col width="40%"/>
<col width="10%"/>
</colgroup>

<thead>

<tr>
<th></th>
<th><p>External tools</p></th>
<th></th>
<th></th>
</tr>

</thead>

<tbody>

<!-- audio player -->
<tr>
<th><xsl:if test="$data/AudioExecPath != ''"><i class="fas fa-check"></i></xsl:if></th>
<th><p><label for="AUDIO_EXEC">mpg123 (audio player)</label></p></th>
<th><p><input type="text" id="AUDIO_EXEC" name="AUDIO_EXEC" value="{$data/AudioExecPath}" required="required" form="setAudioExecPath"></input></p></th>
<th>
<form method="POST" action="app.config.sh" id="setAudioExecPath">
<input type="hidden" name="action" value="setAudioExecPath"></input>
<input type="submit" value="OK"></input>
</form>
</th>
</tr>

<!-- test du son -->
<xsl:if test="$data/AudioExecPath/@status = 'true'">

<tr>
<th></th>
<th><p><label for="AUDIO_SAMPLE_PLAY">Play test sound</label></p></th>
<th></th>
<th>
<form method="POST" action="app.config.sh" id="playAudioSample">
<input type="hidden" name="action" value="playAudioSample"></input>
<input type="submit" value="Play"></input>
</form>
</th>
</tr>

</xsl:if>

</tbody>
</table>

<!-- Cas des mises à jour -->
<xsl:if test=" application/@hasUpdates = 'true'">
<hr class="app-separe"/>
<table class="app-display">

<colgroup>
<col width="10%"/>
<col width="40%"/>
<col width="40%"/>
<col width="10%"/>
</colgroup>

<thead>

<tr>
<th></th>
<th><p>Updates</p></th>
<th></th>
<th></th>
</tr>

</thead>

<tbody>

<!-- Mises à jour core -->
<tr>
<th></th>
<th colspan="2"><p>The application is ready to be updated.</p></th>
<th>
<form method="POST" action="app.config.sh" id="DeployUpdates">
<input type="hidden" name="action" value="DeployUpdates"></input>
<input type="submit" value="Update"></input>
</form>
</th>
</tr>

</tbody>

</table>

</xsl:if>

<!-- Cas des requêêtes -->
<xsl:if test="count($data/requests/request) &gt; 0">

<hr class="app-separe"/>
<table class="app-display">

<colgroup>
<col width="10%"/>
<col width="90%"/>
</colgroup>

<thead>

<tr>
<th></th>
<th><p>Requests' history</p></th>
</tr>

</thead>

<tbody>

<xsl:for-each select="$data/requests/request">
<tr>
<th><p><xsl:value-of select="@lang"></p></th>
<th><p><xsl:value-of select="."></p></th>
</tr>
</xsl:for-each>

</tbody>

</table>

</xsl:if>

</xsl:if>
</main>
</xsl:template>

</xsl:stylesheet>
