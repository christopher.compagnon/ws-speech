#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Speech webservice
#@(#)--------------------------------------------------------------
#@(#) Webservice for calling tts
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
# Chargement de la définition des variables
. ./app.etc.sh
# Chargement des paramètres de l'application
. ${CONFIG_FILE}

#------------------------------------------------------------------
hashsum () {
cat - | openssl dgst -sha256 | sed 's/ //g' | cut -d '=' -f2-
}

# Envoie une réponse au client
send_http_response () {
# Send an HTTP error and exit
echo "Status: ${HTTP_CODE} ${HTTP_MSG}"
echo "Date: $(date -u -R)"
echo 'Content-type: text/plain'
echo
echo "CODE=${HTTP_CODE}"
echo "MESSAGE=${HTTP_MSG}"
exit 0
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
LOG_FILE="${LOG_DIR}/${SCRIPT_NAME}.$(date -u '+%Y%m%d').log"
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Traitement de la pénalité
USER_SIGN="$(echo "${REMOTE_ADDR}.${HTTP_USER_AGENT:-none}" | hashsum)"
# Vérification de la pénalité
DELAY=$(find ${DATA_TMP}/delay/ -maxdepth 1 -type f -name "${USER_SIGN}.*.delay" -cmin -5 -print | wc -l)
[ ${DELAY:-0} -gt 0 ]  && sleep ${DELAY:-0}
# Effacer les vieux fichiers de pénalité
find ${DATA_TMP}/delay/ -maxdepth 1 -type f -name "${USER_SIGN}.*.delay" -cmin +5 -print | xargs rm -f
#------------------------------------------------------------------
# Traitement du formulaire
[ "${REQUEST_METHOD}" = "POST" ] && read QUERY_STRING

# Déchiffrement du message
MSG_STRING="$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^msg= | cut -d'=' -f2- | openssl aes-256-cbc -d -salt -pbkdf2 -base64 -k "${PASSCODE}")"

# Récupération des paramètres
TOKENID="$(echo "${MSG_STRING}" | tr '&' '\n' | grep ^TOKENID= | cut -d'=' -f2)"
LANG="$(echo "${MSG_STRING}" | tr '&' '\n' | grep ^LANG= | cut -d'=' -f2)"
TEXT="$(echo "${MSG_STRING}" | tr '&' '\n' | grep ^TEXT= | cut -d'=' -f2-)"

# Contrôles
[ "${TOKENID:-none}" = "none" ] && {
# Création d'une pénalité
mkdir -p ${DATA_TMP}/delay
touch ${DATA_TMP}/delay/${USER_SIGN}.$(date '+%Y%m%d%H%M%S').delay
# Envoi de la réponse
HTTP_CODE="401"
HTTP_MSG="Unauthorized"
send_http_response
}

[ "${TEXT:-none}" = "none" ] && {
HTTP_CODE="400"
HTTP_MSG="Bad Request"
send_http_response
}

[ "${LANG:-none}" = "none" ] && {
HTTP_CODE="400"
HTTP_MSG="Bad Request"
send_http_response
}
#------------------------------------------------------------------
# Lecture du texte (tts)
echo "$(date -u) - ${TOKENID} -- ${LANG} / ${TEXT}" >> ${DATA_LOG_FILE}
${APP_BIN_DIR}/tts.sh -l ${LANG} -t "${TEXT}"

HTTP_CODE="200"
HTTP_MSG="OK"
send_http_response
#------------------------------------------------------------------
# Nettoyage
#ls -1t ${LOG_DIR}/*.log | awk -v NB_BCK=${LOG_RETENTION_MAX:-30} 'NR>NB_BCK {print $0}' | xargs rm -f
find ${LOG_DIR} -type f -name "*.log" -mtime +${LOG_RETENTION_MAX:-30} -print | xargs rm -f
#------------------------------------------------------------------
exit 0
