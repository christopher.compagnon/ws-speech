#!/bin/sh
# Configuration de l'application
#set -x
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

genRandomCode () {
LENGTH=${1}
dd if=/dev/urandom count=1 | openssl base64 | tr -d '\n' | sed 's/+//g' | sed 's/\///g' | cut -c-${LENGTH:-40}
}

hashPass () {
HASH="$(echo "${1}" | openssl dgst -sha256 | sed 's/ //g' | cut -d '=' -f2-)"
# Retourne la valeur
echo "{SHA256}${HASH}=="
}

hashsum () {
cat - | openssl dgst -sha256 | sed 's/ //g' | cut -d '=' -f2-
}

# Envoie une réponse au client
send_http_response () {
# Send an HTTP error and exit
echo "Status: ${HTTP_CODE} ${HTTP_MSG}"
echo "Date: $(date -u -R)"
echo 'Content-type: text/html'
echo
echo "<html><head><title>${HTTP_MSG}</title></head><body><h1>${HTTP_CODE} - ${HTTP_MSG}</h1></body></html>"
exit 0
}

ConvertIP2Binary () {
# Découpage des blocs d'adresse IPv4
IPv4_1=$(echo "${1}" | cut -d"." -f1)
IPv4_2=$(echo "${1}" | cut -d"." -f2)	
IPv4_3=$(echo "${1}" | cut -d"." -f3)	
IPv4_4=$(echo "${1}" | cut -d"." -f4)

# Conversion de l'adresse en binaire
IPv4_1b=$(echo "ibase=10; obase=2 ; ${IPv4_1}" | bc | xargs printf "%08d\n")
IPv4_2b=$(echo "ibase=10; obase=2 ; ${IPv4_2}" | bc | xargs printf "%08d\n")
IPv4_3b=$(echo "ibase=10; obase=2 ; ${IPv4_3}" | bc | xargs printf "%08d\n")
IPv4_4b=$(echo "ibase=10; obase=2 ; ${IPv4_4}" | bc | xargs printf "%08d\n")
echo "${IPv4_1b}${IPv4_2b}${IPv4_3b}${IPv4_4b}"
}


WriteEtc () {
# Écriture de la configuration
echo "# $(date -u '+%c')" > ${ETC_FILE}
echo "# Vault" >> ${ETC_FILE}
echo "VAULT_USAGE=\"${VAULT_USAGE:-false}\"" >> ${ETC_FILE}
echo "VAULT_FILE=\"${VAULT_FILE}\"" >> ${ETC_FILE}
echo "# Configuration" >> ${ETC_FILE}
echo "CONFIG_FILE=\"${CONFIG_FILE}\"" >> ${ETC_FILE}
}

WriteVault () {
# Écriture du fichier vault
echo "# This file should have ro access only for the web server user after configuration" > ${VAULT_FILE}
echo "SERVER_ID=\"${SERVER_ID}\"" >> ${VAULT_FILE}	
echo "KEY_PASSPHRASE=\"${KEY_PASSPHRASE}\"" >> ${VAULT_FILE}
}

WriteConfig () {
# Sauvegarde de l'ancienne configuration
cp ${CONFIG_FILE} ${CONFIG_FILE}.$(date -u +'%Y%m%d%H%M%S')
mv ${CONFIG_FILE}.* ${APP_ARCH_DIR}/

# Écriture de la configuration
echo "# $(date -u '+%c')" > ${CONFIG_FILE}
echo "# Variables globales" >> ${CONFIG_FILE}

echo "ENDPOINT_STATUS=\"${ENDPOINT_STATUS:-false}\"" >> ${CONFIG_FILE}
echo "APP_URL=\"${APP_URL}\"" >> ${CONFIG_FILE}
echo "SERVER_ID=\"${SERVER_ID}\"" >> ${CONFIG_FILE}
echo "LOG_RETENTION_MAX=${LOG_RETENTION_MAX:-30}" >> ${CONFIG_FILE}

# Accès IPv4
echo "CONFIG_ADDR_RESTRICTION=\"${CONFIG_ADDR_RESTRICTION:-false}\"" >> ${CONFIG_FILE}
echo "CONFIG_ADDR_V4_VALUE=\"${CONFIG_ADDR_V4_VALUE}\"" >> ${CONFIG_FILE}
echo "CONFIG_ADDR_V4_MASK=\"${IPv4_mask}\"" >> ${CONFIG_FILE}
echo "BINARY_ADDR_V4_VALUE=\"${BINARY_ADDR_V4_VALUE}\"" >> ${CONFIG_FILE}

echo "#------------------------------------------------------------------" >> ${CONFIG_FILE}
echo "# Répertoires applicatifs" >> ${CONFIG_FILE}
echo "APP_DIR=\"${APP_DIR}\"" >> ${CONFIG_FILE}
echo "APP_LOG_DIR=\"${APP_DIR}/logs\"" >> ${CONFIG_FILE}
echo "APP_BIN_DIR=\"${APP_DIR}/bin\"" >> ${CONFIG_FILE}
echo "APP_ARCH_DIR=\"${APP_DIR}/archives\"" >> ${CONFIG_FILE}
echo "APP_WEBAPP_DIR=\"$(pwd)\"" >> ${CONFIG_FILE}
echo "APP_LOG_FILE=\"${APP_LOG_DIR}/\$(date -u +'%Y%m%d').log\"" >> ${CONFIG_FILE}

echo "#------------------------------------------------------------------" >> ${CONFIG_FILE}
echo "# Versions" >> ${CONFIG_FILE}
echo "CORE_VERSION_FILE=\"${APP_DIR}/core.version\"" >> ${CONFIG_FILE}
echo "BIN_VERSION_FILE=\"${APP_DIR}/bin.version\"" >> ${CONFIG_FILE}
echo "WEBAPP_VERSION_FILE=\"${APP_DIR}/webapp.version\"" >> ${CONFIG_FILE}

echo "#------------------------------------------------------------------" >> ${CONFIG_FILE}
echo "# Variables de travail" >> ${CONFIG_FILE}
echo "LOG_DIR=\"${APP_DIR}/work/logs\"" >> ${CONFIG_FILE}
echo "ARCH_DIR=\"${APP_DIR}/work/archives\"" >> ${CONFIG_FILE}
echo "DATA_TMP=\"${APP_DIR}/work/tmp\"" >> ${CONFIG_FILE}
echo "DATA_DIR=\"${APP_DIR}/work/data\"" >> ${CONFIG_FILE}
echo "NOTIFY_DIR=\"\${DATA_TMP}\"" >> ${CONFIG_FILE}
echo "DATA_LOG_FILE=\"${LOG_DIR}/\$(date -u +'%Y%m%d').log\"" >> ${CONFIG_FILE}

echo "#------------------------------------------------------------------" >> ${CONFIG_FILE}
echo "# Programmes externes" >> ${CONFIG_FILE}
echo "AUDIO_EXEC=\"${AUDIO_EXEC}\"" >> ${CONFIG_FILE}

echo "#------------------------------------------------------------------" >> ${CONFIG_FILE}
echo "# Paramètres supplémentaires" >> ${CONFIG_FILE}
echo "PASSCODE=\"${PASSCODE}\"" >> ${CONFIG_FILE}
}

WriteLock () {

# Écriture des données dans le verrou
echo "# $(date -u '+%c')" > ${LOCK_FILE}
echo "LOCKED_BY=${SESSIONID:-false}" >> ${LOCK_FILE}
echo "APP_DIR_STATUS=${APP_DIR_STATUS:-false}" >> ${LOCK_FILE}
echo "APP_URL_STATUS=${APP_URL_STATUS:-false}" >> ${LOCK_FILE}
echo "AUDIO_EXEC_STATUS=${AUDIO_EXEC_STATUS:-false}" >> ${LOCK_FILE}
}

InfoUnlock () {
# Déverrouillage des informations --> lecture + écriture
touch ${LOCK_FILE}
}

InfoLock () {
# Verrouillage des informations --> lecture seule
[ -f ${LOCK_FILE} ] && mv ${LOCK_FILE} ${UNLOCK_FILE}

# Recopie du fichier dans le répertoire applicatif
cp ${ETC_FILE} ${APP_BIN_DIR}/
}

hasChanged () {
# Détermine si la configuration a changé depuis moins de 15 minutes
#find ${APP_DIR} -type f -name "endpoint.conf" -mtime -1 -print | wc -l
find ${APP_DIR} -type f -name "endpoint.conf" -mmin -15 -print | wc -l
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
APP_VERSION_NUMBER="0.1a"
APP_VERSION_NAME="Aaron"

IPv4_CHECK=$(echo ${REMOTE_ADDR} | awk -F'.' '{print NF}')
IPv6_CHECK=$(echo ${REMOTE_ADDR} | awk -F':' '{print NF}')
[ ${IPv4_CHECK:-0} -eq 4 ] && REMOTE_ADDR_VERSION=4
[ ${IPv6_CHECK:-0} -gt 3 ] && REMOTE_ADDR_VERSION=6
#------------------------------------------------------------------
# Traitement du cookie
SessionToken=$(echo ${HTTP_COOKIE} | tr ';' '\n' | grep SessionToken= | cut -d"=" -f2-)
[ "${SessionToken:-none}" = "none" ] && SessionToken="$(date -u '+%Y%m%d%H%M%S')$$"
echo "Set-Cookie: SessionToken=${SessionToken};"
SESSIONID=$(echo "${REMOTE_ADDR}${HTTP_USER_AGENT}${SessionToken}" | openssl dgst -sha256 | cut -d'=' -f2- | sed 's/ //g')
USER_SIGN="$(echo "${REMOTE_ADDR}.${HTTP_USER_AGENT:-none}" | hashsum)"
# [TODO] : ajouter la gestion du token/SESSIONID dans le verrou de connexion

# Vérification d'une session existante
#NB_SESSION_FILE=$(ls -1 ./)

ETC_FILE="./app.etc.sh"
#LOCK_FILE="./app.lock.sh"
#UNLOCK_FILE="./app.unlock"
APP_BASE_DIR="/var/tmp"
# [TODO] : et si le répertoire n'existe pas ? --> créer en local et demander un répertoire d'installation

# Si le fichier de base n'existe pas, on le crée
[ ! -f ${ETC_FILE} -a -d ${APP_BASE_DIR} ] && {
SERVER_ID="$(genRandomCode 40)"
PASSCODE="$(genRandomCode 12)"
APP_DIR="${APP_BASE_DIR}/speech/${SERVER_ID}"
mkdir -p ${APP_DIR}

# Définition de APP_URL par défaut
NB_FIELDS=$(echo "${HTTP_REFERER}" | awk -F '/' '{print NF}')
APP_URL_ROOT=$(expr ${NB_FIELDS:-0} - 1)
[ ${APP_URL_ROOT:-0} -gt 1 ] && APP_URL="$(echo "${HTTP_REFERER}" | cut -d'/' -f 1-${APP_URL_ROOT})"

# Teste l'existence des programmes
AUDIO_EXEC=$(whereis mpg123 | awk '{print $2}')

CONFIG_FILE=${APP_DIR}/endpoint.conf
VAULT_FILE=${APP_DIR}/vault.conf
WriteEtc
WriteVault

# Création des répertoires
mkdir -p ${APP_DIR}/logs
mkdir -p ${APP_DIR}/bin
mkdir -p ${APP_DIR}/archives

mkdir -p ${APP_DIR}/work/logs
mkdir -p ${APP_DIR}/work/archives
mkdir -p ${APP_DIR}/work/tmp
mkdir -p ${APP_DIR}/work/data
WriteConfig

# Création des fichiers par défaut

# [TODO] : copie des binaires dans le répertoire APP_BIN_DIR
# [TODO] : génération d'un fichier Vault d'exemple
# [TODO] : génération des répertoires dans l'application web
# [TODO] : ajouter la possibilité de vérifier l'installation après la première fois (pour gérer l'installation différée)
}

# Chargement des paramètres globaux de l'application
. ${ETC_FILE}
. ${CONFIG_FILE}
. ${VAULT_FILE}
#------------------------------------------------------------------
# Vérification de la pénalité
DELAY=$(find ${DATA_TMP}/delay/ -maxdepth 1 -type f -name "${USER_SIGN}.*.delay" -cmin -5 -print | wc -l)
[ ${DELAY:-0} -gt 0 ] && sleep ${DELAY:-0}
# Si trop de fichier delay, alors attaque possible, donc tout bloquer
ALL_DELAY=$(find ${DATA_TMP}/delay/ -maxdepth 1 -type f -name "*.*.delay" -cmin -5 -print | wc -l)
[ ${ALL_DELAY:-0} -gt 50 ] && {
HTTP_CODE="503"
HTTP_MSG="Service Unavailable"
send_http_response
}
# Effacer les vieux fichiers de pénalité
find ${DATA_TMP}/delay/ -maxdepth 1 -type f -name "${USER_SIGN}.*.delay" -cmin +5 -print | xargs rm -f
#------------------------------------------------------------------
# contrôle d'accès IP
REMOTE_ADDR_BINARY=$(ConvertIP2Binary "${REMOTE_ADDR}" | cut -c1-${CONFIG_ADDR_V4_MASK:-24})
CONTROL_ADDR_BINARY=$(echo "${BINARY_ADDR_V4_VALUE}" | cut -c1-${CONFIG_ADDR_V4_MASK:-24})
# Comparaison avec la référence
[ "${CONFIG_ADDR_RESTRICTION:-false}" = "true" -a "${REMOTE_ADDR_BINARY:-none}" != "${CONTROL_ADDR_BINARY:-enon}" ] && echo '<notification type="MsgIPInvalidError"/>' >> ${NOTIFY_DIR}/${SESSIONID}.msg.xml
# [TODO] : mettre l'accès en erreur 403 si l'IP est invalide
# [TODO] : si tentative de connexion depuis une mauvaise adresse IP --> alerte dans le flux de syndication
#------------------------------------------------------------------
# Verrou
LOCK_FILE="${APP_DIR}/work/tmp/app.lock"
UNLOCK_FILE="${APP_DIR}/work/tmp/app.unlock"

[ -f ${LOCK_FILE} ] && LOCKED=true
[ -f ${LOCK_FILE} ] && . ${LOCK_FILE}
#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Protection : Verrouille l'interface si aucune modification récente
HAS_CHANGED=$(hasChanged)
[ -f ${CONFIG_FILE} -a -f ${LOCK_FILE} -a ${HAS_CHANGED:-0} -eq 0 ] && InfoLock && echo '<notification type="MsgSessionExpiredError"/>' >> ${NOTIFY_DIR}/${SESSIONID}.msg.xml

# Protection : Verrouille l'interface si le verrou ne correspond pas à la Session
[ -f ${LOCK_FILE} -a "${LOCKED_BY:-none}" != "${SESSIONID}" ] && InfoLock && echo '<notification type="MsgSessionInvalidError"/>' >> ${NOTIFY_DIR}/${SESSIONID}.msg.xml
# [TODO] : mettre en place une erreur de mauvaise authentification pour le cas ddu verrou non conforme
# [TODO] : mettre en place la décompression/installation du core

# Traitement du formulaire
[ "${REQUEST_METHOD}" = "POST" ] && read QUERY_STRING

ACTION=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^action= | cut -d"=" -f2)

case ${ACTION} in

setVault)
# Définition du Vault
VAULT_FILE=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^VAULT_FILE= | cut -d"=" -f2- | tr '+' ' ')

# Test de l'existance du Vault
[ ! -f ${VAULT_FILE} ] && NO_VAULT_FILE=true
[ "${NO_VAULT_FILE:-false}" = "true" ] && {
VAULT_USAGE=true
WriteEtc
WriteConfig
}

# Rechargement des paramètres globaux de l'application
. ${ETC_FILE}
. ${CONFIG_FILE}
# [TODO] : un fichier Vault dans tous les cas ?
;;

defineConnection)
# Définition du mot de passe
Passphrase1=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^Passphrase1= | cut -d"=" -f2- | tr '+' ' ')
Passphrase2=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^Passphrase2= | cut -d"=" -f2- | tr '+' ' ')

# [TODO] : cas du mot de passe invalide
[ "${Passphrase1:-none}" != "${Passphrase2:-enon}" ] && echo "Invalid passphrase !" >> ${APP_LOG_FILE}
[ "${Passphrase1:-none}" = "${Passphrase2:-enon}" ] && {
KEY_PASSPHRASE="${Passphrase1}"
WriteVault

ENDPOINT_STATUS=true
WriteConfig
# Déverrouillage de l'interface
InfoUnlock
}
[ -f ${LOCK_FILE} ] && LOCKED=true
# Rechargement des paramètres globaux de l'application
. ${VAULT_FILE}
. ${CONFIG_FILE}
;;

getConnection)
# Connexion
Passphrase1=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^Passphrase1= | cut -d"=" -f2- | tr '+' ' ')
if [ "${Passphrase1:-none}" = "${KEY_PASSPHRASE:-enon}" ] 
then
InfoUnlock
LOCKED=true
WriteLock
# Effacer les fichiers de pénalité
find ${DATA_TMP}/delay/ -maxdepth 1 -type f -name "${USER_SIGN}.*.delay" -print | xargs rm -f
# Met à jour le fichier de config pour bloquer l'affichage
touch ${CONFIG_FILE}
else
LOCKED=false
echo '<notification type="MsgPassphraseInvalidError"/>' >> ${NOTIFY_DIR}/${SESSIONID}.msg.xml
# Création d'une pénalité
mkdir -p ${DATA_TMP}/delay
touch ${DATA_TMP}/delay/${USER_SIGN}.$(date '+%Y%m%d%H%M%S').delay
fi
;;

unlock)
# Verrouillage en lecture seule --> suppression du verrou
InfoLock
LOCKED=false
;;

setAppDir)
APP_DIR=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^APP_DIR= | cut -d"=" -f2- | tr '+' ' ' | sed 's/%2F/\//g')

# Test de la configuration
[ ! -d ${APP_DIR} ] && NO_APP_DIR=true
[ -d ${APP_DIR} ] && touch ${APP_DIR}/speech.test
[ ! -f ${APP_DIR}/speech.test ] && NO_APP_RIGHTS=true
[ -f ${APP_DIR}/speech.test ] && rm -f ${APP_DIR}/speech.test

[ "${NO_APP_DIR:-false}" = "false" -a "${NO_APP_RIGHTS:-false}" = "false" ] && {
# Si la configuration est OK, alors écriture
APP_DIR_STATUS=true
WriteConfig
WriteLock

}

# Rechargement des paramètres globaux de l'application
. ${CONFIG_FILE}
if [ "${LOCKED:-false}" = "true" ]
then
. ${LOCK_FILE}
fi

;;

setEndPointURL)
APP_URL=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^APP_URL= | cut -d"=" -f2- | tr '+' ' ' | sed 's/%2F/\//g' | sed 's/%3A/:/g')
# [TODO] : si la valeur change, il faut déplacer le répertoire dans le nouvel emplacement
# Modification des URL des comptes locaux
#APP_URL="${NEW_APP_URL}"

# Si la configuration est OK, alors écriture
APP_URL_STATUS=true
WriteConfig
WriteLock

# Rechargement des paramètres globaux de l'application
. ${CONFIG_FILE}
if [ "${LOCKED:-false}" = "true" ]
then
. ${LOCK_FILE}
fi
;;


setAudioExecPath)
# Définition du at
AUDIO_EXEC=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^AUDIO_EXEC= | cut -d"=" -f2- | tr '+' ' ' | sed 's/%2F/\//g')
[ -f ${AUDIO_EXEC} ] && {
# Si la configuration est OK, alors écriture
AUDIO_EXEC_STATUS=true
WriteConfig
WriteLock
}

# Rechargement des paramètres globaux de l'application
. ${CONFIG_FILE}
if [ "${LOCKED:-false}" = "true" ]
then
. ${LOCK_FILE}
fi
;;


setMaxLogsRetention)
# Définition du logs à retenir
LOG_RETENTION_MAX=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^LOG_RETENTION_MAX= | cut -d"=" -f2-)
WriteConfig

# Rechargement des paramètres globaux de l'application
. ${CONFIG_FILE}
;;


setIPRestriction)
IPv4_1=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^IPv4_1= | cut -d"=" -f2)
IPv4_2=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^IPv4_2= | cut -d"=" -f2)
IPv4_3=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^IPv4_3= | cut -d"=" -f2)
IPv4_4=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^IPv4_4= | cut -d"=" -f2)
IPv4_mask=$(echo ${QUERY_STRING} | tr '&' '\n' | grep ^IPv4_mask= | cut -d"=" -f2)
CONFIG_ADDR_RESTRICTION="false"

# Vérification de la configuration IPv4
[ ${IPv4_mask:-0} -gt 0 -a ${IPv4_mask:-0} -le 32 -a ${IPv4_1:-0} -gt 0 -a ${IPv4_1:-0} -le 255 -a ${IPv4_2:-0} -ge 0 -a ${IPv4_2:-0} -le 255 -a ${IPv4_3:-0} -ge 0 -a ${IPv4_3:-0} -le 255 -a ${IPv4_4:-0} -ge 0 -a ${IPv4_4:-0} -le 255 ] && {
CONFIG_ADDR_RESTRICTION="true"
CONFIG_ADDR_V4_VALUE="${IPv4_1}.${IPv4_2}.${IPv4_3}.${IPv4_4}"
BINARY_ADDR_V4_VALUE=$(ConvertIP2Binary "${CONFIG_ADDR_V4_VALUE}")

WriteConfig
echo "$(date -u '+%c') - ${SessionToken} -- IPv4 restriction is active on ${CONFIG_ADDR_V4_VALUE}/${IPv4_mask}" >> ${APP_LOG_FILE}
}
# Rechargement des paramètres globaux de l'application
. ${CONFIG_FILE}
;;

GenNewPassode)
echo "$(date -u '+%c') - ${SessionToken} -- Generates new random passcode" >> ${APP_LOG_FILE}
PASSCODE="$(genRandomCode 12)"
WriteConfig
;;

playAudioSample)
# Joue un échantillon de son pour tester la config
AUDIO_DIR="${DATA_DIR}/audio"
AUDIO_FILE="${AUDIO_DIR}/Hello.mp3"
[ "${AUDIO_EXEC:-none}" != "none" ] && ${AUDIO_EXEC} -q "${AUDIO_FILE}" 2>> ${APP_LOG_FILE}
;;

DeployUpdates)
echo "$(date -u '+%c') - ${SessionToken} -- Launches Updates…" >> ${APP_LOG_FILE}
# [TODO] : archivage de l'ancienne version
[ -f core.tar ] && {
# Décompression du paquet
tar -xf core.tar -C "${DATA_TMP}"

# Pour chaque fichier listé 
cat app.update | grep -v "^#" | while read FILE
do
# Copie du fichier dans l'arborescence
#echo "Copy ${FILE}" >&2
cp -R "${DATA_TMP}/core/${FILE}" "${APP_DIR}/${FILE}"
done

chmod +x ${APP_BIN_DIR}/*.sh
[ -f core.tar ] && rm -f core.tar
[ -d "${DATA_TMP}/core" ] && rm -fR "${DATA_TMP}/core"
echo "$(date -u '+%c') - ${SessionToken} -- Update of the core" >> ${APP_LOG_FILE}

}

# Effacement des mises à jour
[ -f app.update ] && rm -f app.update

# [TODO] : créer une notification pour indiquer que la mise à jour est déployée
echo '<notification type="MsgUpdateDeployedSuccess"/>' >> ${NOTIFY_DIR}/${SESSIONID}.msg.xml
echo "$(date -u '+%c') - ${SessionToken} -- Updates done" >> ${APP_LOG_FILE}
;;

*)
;;

esac

# Cas de la mise à jour
[ -f app.update ] && HAS_UPDATES="true"
# Cas des notifications
[ -f ${NOTIFY_DIR}/${SESSIONID}.msg.xml ] && HAS_NOTIFICATIONS="true"
# Index des requêtes
INDEX_FILE="${DATA_DIR}/audio/audio.idx"
#------------------------------------------------------------------
# Cookie
#echo "Set-Cookie: ContextParamEdit=${ContextParamEdit};"


# En-tête fichier
echo "Content-type: application/xml; charset=utf-8"
echo
#------------------------------------------------------------------
#Génération du XML
echo '<?xml version="1.0" encoding="UTF-8"?>'
echo '<?xml-stylesheet type="text/xsl" href="app.config.xsl"?>'
echo "<application timestamp=\"$(date -u '+%Y%m%d%H%M%S')\" hasUpdates=\"${HAS_UPDATES:-false}\">"
echo '<params>'
echo "<action>${ACTION:-none}</action>"
echo "<locked>${LOCKED:-false}</locked>"
echo "<AllowUpdates>${ALLOW_UPDATES:-false}</AllowUpdates>"
echo "<AccessRestrictionByIP>${CONFIG_ADDR_RESTRICTION:-false}</AccessRestrictionByIP>"
echo '</params>'
echo '<data>'

echo "<EndPointID status=\"${ENDPOINT_STATUS:-false}\">${SERVER_ID}</EndPointID>"
echo "<Vault status=\"${VAULT_USAGE:-false}\">${VAULT_FILE}</Vault>"

[ "${LOCKED:-false}" = "true" ] && {
echo "<AppDir status=\"${APP_DIR_STATUS:-false}\">${APP_DIR}</AppDir>"
echo "<EndPointURL status=\"${APP_URL_STATUS:-false}\">${APP_URL}</EndPointURL>"
echo "<RemoteAddress version=\"${REMOTE_ADDR_VERSION:-0}\">${REMOTE_ADDR}</RemoteAddress>"
echo "<IPv4AddressRestriction mask=\"${CONFIG_ADDR_V4_MASK:-24}\">${CONFIG_ADDR_V4_VALUE}</IPv4AddressRestriction>"

echo "<MaxLogsRetention>${LOG_RETENTION_MAX:-30}</MaxLogsRetention>"
echo "<AudioExecPath status=\"${AUDIO_EXEC_STATUS:-false}\">${AUDIO_EXEC}</AudioExecPath>"
echo "<Passcode>${PASSCODE}</Passcode>"

# Affichage des dernières requêtes
[ -f ${INDEX_FILE} ] && {
echo "<requests>"
tac ${INDEX_FILE} | grep -v "^#" | head -20 | awk -F'|' '{print "<request hash=\""$1"\" lang=\""$2"\">"$3"</request>"}'
echo "</requests>"
}

echo '</data>'
[ "${HAS_NOTIFICATIONS:-false}" = "true" ] && {
echo '<notifications>'
cat ${NOTIFY_DIR}/${SESSIONID}.msg.xml
echo '</notifications>'
}
echo '</application>'
#------------------------------------------------------------------
# Effacement des notifications
[ "${HAS_NOTIFICATIONS:-false}" = "true" ] && rm -f ${NOTIFY_DIR}/${SESSIONID}.msg.xml
#------------------------------------------------------------------

exit 0
