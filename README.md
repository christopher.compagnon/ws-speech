# ws-speech

**ws-speech** is a web-service for Speech, client/server version of a tts tool which reads notifications.
It can be used for server/application/infrastructure monitoring, or piping actions with vocal control.

**Note** : This version has been developped and tested on Linux Debian/Ubuntu. Feel free to adapt for your usage/operating system.


## Material

- A server (like a Raspberry Pi with Debian or Ubuntu Server installed);
- Speakers wired to the server.

## Installation

### Dependancies

- **Apache** : web server

    apt install apache2

- **mpg123** : a simple command line audio player for playing the mp3 sounds.

    apt install mpg123

- **bc** : a calculator (required for some operations)

    sudo apt install bc

Or all in one command line :

    sudo apt install apache2 mpg123 bc

## Configuration

### Volume

The volume of the system can be set by ALSA mixer :

    sudo amixer controls

For example, if the execution returns :

    numid=2,iface=MIXER,name='Headphone Playback Switch'
    numid=1,iface=MIXER,name='Headphone Playback Volume'

Find the numid of Playback Volume (here numid=1), and set the volume higher or lower with :

    sudo amixer cset numid=1 100%

Here, the volume is set to the max (100%).


### mpg123

By default, the user cannot execute mpg123. You need to be root (sudo …).
You have to authorize www-data to play the sound.

    sudo usermod -a -G audio www-data

and eventually reboot, then test on behalf Apache :

    sudo -u www-data mpg123 my_file.mp3

If the sound is played, it is OK.

### Web server (Apache)

Open the Apache configuration */etc/apache2/sites-available/000-default.conf* (or create a new conf and activate it with *a2ensite* command), and add :

    <Directory "/var/www/html">
    Options +Indexes +FollowSymLinks +ExecCGI +SymLinksIfOwnerMatch -MultiViews
    AddHandler cgi-script .sh
    DirectoryIndex app.config.sh

    AllowOverride None
    Require all granted
    </Directory>

Replace */var/www/html* by the path to the installation in Apache data directory. It can be */var/www/ws-speech*. Save the file.

Activate the cgi :

    sudo a2enmod cgid

By default, for security reasons, systemd securizes Apache in a virtual directory systemd-private…
You have to unlock this security fr storing files permanently and irteract with the system.

Open the configuration file */etc/systemd/system/multi-user.target.wants/apache2.service*, and switch the *PrivateTmp* parameter from *true* to *false*.


Reload systemd :

    sudo systemctl daemon-reload

Restart the web server :

    sudo service apache2 restart

Connect to you web server from a web browser.

### Application

Download th last {version} of the sources from the *builds* directory :

    wget https://gitlab.com/christopher.compagnon/ws-speech/-/blob/master/builds/ws-speech-{version}.tar.xz

Uncompress :

    tar -xJf ./ws-speech-{version}.tar.xz

Copy the directory to the Apache data directory :

    sudo cp -R ./ws-speech-{version}/ /var/www/
    sudo mv /var/www/html /var/www/html.old
    sudo mv /var/www/ws-speech-{version} /var/www/html 

Change the rights :

    sudo chown -R www-data:www-data /var/www/html

of the web folder. If the web server is correctly configured, the application is accessible.

Open your browser, connect to the app.config.sh page. For example : http://ip_address_of_server/speech/app.config.sh.

The page is designed to configure the webservice.
The options are :

- **IP restriction (for web interface)** : configuration on IPv4 accesse restriction for administrator.
- **Public URL** : the ip address of the application. In our example, you should use http://ip_address_of_server/speech.
- **Logs retention (days)** : for setting the duration on logs.
- **Passcode (authentication for webservice)** : this code is used for the client to send un message. You can change it by cliking on "Refresh".
- **mpg123 (audio player)** : check or define the path to mpg123 (See installation).
- **Play test sound** : play a sound to check if the configuration is OK. 

Once configured, the web-service is available at http://ip_address_of_server/speech/ws.speech.sh.

## Usage

You can download a client (for Linux too) for test or using the web-service.
This client is provided with ws-speech, downloadable from the configuration interface.
Please ask the usage with :

    ./client.sh -h

To request a tts :

    ./client.sh -l en -t "Hello world!" -p "Passcode" -u "http://ip_address_of_server/speech/ws.speech.sh"

Where *Passcode* is the same code provided by the configuration, option *Passcode*.

Once the request is sent, the *passcode* (-p) and the *url* (-u) are not required, except if any modification.
Simply call :

    ./client.sh -l en -t "Hello again!"

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")
