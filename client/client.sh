#!/bin/sh
#set -x
SCRIPT_DIR="$(dirname ${0})"
SCRIPT_NAME="$(basename ${0})"
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) Client for calling the Speech webservice
#@(#)--------------------------------------------------------------
#@(#) Client for calling the Speech webservice
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------

print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

usage () {
echo "Usage : ${SCRIPT_NAME} [ -l language ] [ -t text ] [ -p passcode ] [ -h ]"
echo "-l (language) : ISO code of the language."
echo "-t (text) : text to speech"
echo "-p (passcode) : passcode for requesting the webservice."
echo "-u (URL) : URL of the webservice."
echo "-h (help) : display help."
}

#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------
TOKENID="$(echo ${SCRIPT_NAME}$(date +'%s')$$ | openssl dgst -sha256 | sed 's/ //g' | cut -d'=' -f2-)"
LANG="$(locale | grep LANGUAGE | cut -d'=' -f2 | cut -d'_' -f1)"
PARAM_FILE="./speech.conf"
LOG_FILE="./speech.log"
DATA_IN="./"

#------------------------------------------------------------------
# Génère un fichier de config par défaut
[ ! -f "${PARAM_FILE}" ] && {
echo "# Parameters" > "${PARAM_FILE}"
echo "LANG=${LANG}" >> "${PARAM_FILE}"
echo "PASSCODE=" >> "${PARAM_FILE}"
echo "URL=" >> "${PARAM_FILE}"
}

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Récupération des options
while getopts ":l:t:p:u:h" OPTION
do

case "${OPTION}" in

"l")
# LANG
P_LANG="${OPTARG}"
;;

"t")
# TEXT
P_TEXT="${OPTARG}"
;;

"p")
# PASSCODE
P_PASSCODE="${OPTARG}"
;;

"u")
# URL
P_URL="${OPTARG}"
;;

"h")
# Help
usage
exit 0
;;

:)
print_error "Missing argument for -${OPTARG}."
echo
usage
exit 1
;;

?)
print_error "Bad option -${OPTARG}."
echo
usage
exit 2
;;

esac

done
#------------------------------------------------------------------
# Chargement des paramètres
. ${PARAM_FILE}
#------------------------------------------------------------------
# Contrôles
[ "${P_URL:-none}" = "none" -a "${URL:-none}" = "none" ] && print_error "URL not defined!" && usage && exit 1
[ "${P_URL:-none}" = "none" -a "${URL:-none}" != "none" ] && P_URL="${URL}"

[ "${P_LANG:-none}" = "none" -a "${LANG:-none}" = "none" ] && print_error "Language not defined!" && usage && exit 1
[ "${P_LANG:-none}" = "none" -a "${LANG:-none}" != "none" ] && P_LANG="${LANG}"

[ "${P_PASSCODE:-none}" = "none" -a "${PASSCODE:-none}" = "none" ] && print_error "Passcode not defined!" && usage && exit 1
[ "${P_PASSCODE:-none}" = "none" -a "${PASSCODE:-none}" != "none" ] && P_PASSCODE="${PASSCODE}"

[ "${P_TEXT:-none}" = "none" ] && print_error "Text to speech not defined!" && usage && exit 1
#------------------------------------------------------------------
# Mise à jour des paramètres
echo "# Parameters" > "${PARAM_FILE}"
echo "LANG=${P_LANG}" >> "${PARAM_FILE}"
echo "PASSCODE=${P_PASSCODE}" >> "${PARAM_FILE}"
echo "URL=${P_URL}" >> "${PARAM_FILE}"
#------------------------------------------------------------------
# Envoie le message au webservice (format GET)

# 1- Encoder :
MSG_STRING=$(echo "TOKENID=${TOKENID}&LANG=${P_LANG}&TEXT=${P_TEXT}" | openssl aes-256-cbc -e -salt -pbkdf2 -base64 -k "${P_PASSCODE}" | tr -d '\n')

# 2- Appel du webservice
DEPLOY=$(wget -a ${LOG_FILE} -P "${DATA_IN}/" -O "${DATA_IN}/${TOKENID}.log" "${P_URL}/ws.speech.sh?msg=${MSG_STRING}")

# 3- traitement du message de retour
RETURN_CODE="$(cat "${DATA_IN}/${TOKENID}.log" | grep ^CODE= | cut -d'=' -f2)"
[ ${RETURN_CODE:-0} -ne 200 ] && print_error 'Something went wrong! Please, see the logs…' && exit 1 

echo "Request sent successfully!"
#------------------------------------------------------------------
# Nettoyage
[ -f "${DATA_IN}/${TOKENID}.log" ] && rm -f "${DATA_IN}/${TOKENID}.log"
#------------------------------------------------------------------
exit 0
